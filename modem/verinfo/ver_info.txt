{
    "Image_Build_IDs": {
        "adsp": "ADSP.HT.4.1-00088-SDM845-1.229022.1", 
        "aop": "AOP.HO.1.0-00137-SDM845AAAAANAZO-1", 
        "apps": "LA.UM.9.3.r1-02100-sdm845.0-1.408559.1", 
        "boot": "BOOT.XF.2.0-00389-SDM845LZB-1", 
        "btfm": "BTFM.CHE.2.1.4.c2-00027-QCACHROMZ-1", 
        "cdsp": "CDSP.HT.1.1-00121-SDM845-1.255607.4.420439.3", 
        "common": "SDM845.LA.2.0.c1-00006-STD.PROD-1.399256.12.403393.15", 
        "glue": "GLUE.SDM845_LA.1.0-00313-NOOP_TEST-1", 
        "modem": "MPSS.AT.4.0.c2.15-00007-SDM845_GEN_PACK-1.437410.1.446401.1", 
        "slpi": "SLPI.HY.1.2-00060-SDM845AZL-1", 
        "tz": "TZ.XF.5.0.1.c5-00083.1-S845AAAAANAZT-1.408403.1", 
        "video": "VIDEO.VE.5.2-00050", 
        "wapi": "WLAN_ADDON.HL.1.0-00030-CNSS_RMZ_WAPI-1", 
        "wdsp": "WDSP.9340.1.0-00277-W9340AAAAAAAZQ-1", 
        "wgig": "WIGIG.SPR.5.3-00025-WIGIGSWZ-1", 
        "wlan": "WLAN.HL.2.0.c8-00050-QCAHLSWMTPLZ-1.435283.1.441421.1"
    }, 
    "Metabuild_Info": {
        "Meta_Build_ID": "SDM845.LA.2.0.c1-00006-STD.PROD-1.399256.12.403393.15", 
        "Product_Flavor": "asic", 
        "Time_Stamp": "2021-11-25 23:53:01"
    }, 
    "Version": "1.0"
}